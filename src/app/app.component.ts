import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Alert } from 'selenium-webdriver';
import { JsonPipe } from '@angular/common';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  form: FormGroup;
  title = 'FacturaAngular';
  listaProductos = [];


  iva = null;
  total = null;

  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      nombreProducto: [null, [Validators.required]],
      precioProducto: [null, [Validators.required]]
    });
  }

  guardarProducto() {

    if (this.form.valid) {
      const nombreProducto = this.form.get('nombreProducto').value;
      const precioProducto = this.form.get('precioProducto').value;

      const mercado = {
        nombreProducto,
        precioProducto
      };
      this.listaProductos.push(mercado);
      this.resetCampos();
    } else {
      alert('Ingresa todos los datos');
    }

  }

  resetCampos() {
    this.form.get('nombreProducto').reset();
    this.form.get('precioProducto').reset();
  }

  facturarProducto() {
    let subtotal = 0;

    this.listaProductos.forEach(dato => {
      subtotal = subtotal + dato.precioProducto;
    });

    this.iva = subtotal * 0.19;
    this.total = subtotal + this.iva;
    // alert('Lista de Productos' + JSON.stringify(this.listaProductos) + 'el valor total es : ' + total);

  }
}
